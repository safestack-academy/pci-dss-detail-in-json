# PCI DSS Detail in JSON

PCI DSS (v3.2.1) controls, applicable Self-Assessment Questionnaires (SAQ), testing method, and priority level - in a handy JSON format.

Data was aggregated from a few documents on https://www.pcisecuritystandards.org/document_library
* v3.2.1 - Jun 2018 of SAQ A, SAQ A-EP, SAQ D Merchant, SAQ D Service Provider
* v3.2.1 - Jun 2018 of Prioritized Approach Tool

We hope you find this format useful for being able to filter and find out which controls you need to have in place, and how they will be tested - and be able to import them into your project management tool of choice for tracking implementation.
